<?php
namespace Twitter;

require_once (__DIR__ . '/ObjectBase.php');

/**
 * https://dev.twitter.com/docs/platform-objects/users
 * 
 * @var string $withheld_scope When present Indicates whether the content being withheld is the "status" or a "user."
 * @var string $withheld_in_countries When present, indicates a textual representation of
 *  the two-letter country codes this user is withheld from.
 *
 * 
 * @var int $utc_offset Nullable. The offset from GMT/UTC in seconds.
 * @var string $time_zone Nullable. A string describing the Time Zone this user declares themselves within.
 *
 * 
 * @var boolean $is_translator When true, indicates that the user is a participant in Twitter's translator community.
 * @var Status $status Nullable.
 *  If possible, the user's most recent tweet or retweet.
 *  In some circumstances, this data cannot be provided and this field will be omitted, null, or empty.
 *  Perspectival attributes within tweets embedded within users cannot always be relied upon.
 * @var boolean $contributors_enabled Indicates that the user has an account with "contributor mode" enabled,
 *  allowing for Tweets issued by the user to be co-authored by another account.
 *  Rarely true.

 * @var int $listed_count The number of public lists that this user is a member of.
 * @var int $favourites_count The number of tweets this user has favorited in the account's lifetime.

 
 * @var string $url Nullable. A URL provided by the user in association with their profile.
 * @var boolean $default_profile When true, indicates that the user has not altered the theme or background of their user profile.
 * @var boolean $default_profile_image When true, indicates that the user has not uploaded their own avatar
 *  and a default egg avatar is used instead.
 * @var Entities $entities Entities which have been parsed out of the url or description fields defined by the user.

 *  British spelling used in the field name for historical reasons.
 * @var boolean $follow_request_sent Nullable. Perspectival.
 *  When true, indicates that the authenticating user has issued a follow request to this protected user account.
 * 
 * 
 * Deprecated:
 * 
 * @var boolean $following Nullable. Perspectival. Deprecated.
 * When true, indicates that the authenticating user is following this user.
 * Some false negatives are possible when set to "false," but these false negatives are increasingly being represented as "null" instead
 * @var boolean $notifications Nullable. Deprecated. May incorrectly report "false" at times.
 *  Indicates whether the authenticated user has chosen to receive this user's tweets by SMS.
 */
class User extends ObjectBase
{
    /**
     * Indicates the user has enabled the possibility of geotagging their Tweets.
     * This field must be true for the current user to attach geographic data when using POST statuses/update.
     *
     * @return boolean
     */
    public function isGeoEnabled()
    {
    	return $this->data->geo_enabled;
    }

    /**
     * Nullable.
     * The user-defined location for this account's profile.
     * Not necessarily a location nor parseable.
     * This field will occasionally be fuzzily interpreted by the Search service.
     *
     * @return string
     */
    public function getLocation()
    {
    	return $this->data->location ;
    }

    /**
     * Indicates if it's a verified account.
     * 
     * @return boolean
     */
    public function isVerified()
    {
    	return $this->data->geo_enabled ;
    }

    /**
     * Indicates this user has chosen to protect their Tweets.
     * 
     * @return boolean
     */
    public function isProtected()
    {
    	return $this->data->protected ;
    }

    /**
     * The number of followers this account currently has.
     * Under certain conditions of duress, this field will temporarily indicate "0."
     *
     * @return int
     */
    public function getFollowersCount()
    {
    	return $this->data->followers_count;
    }

    /**
     * The number of tweets (including retweets) issued by the user.
     *
     * @return int
     */
    public function getStatusesCount()
    {
    	return $this->data->statuses_count;
    }
 
    /**
     * The UTC datetime that the user account was created on Twitter.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {

        return new \DateTime( $this->data->created_at );
    }

    /**
     * The name of the user, as they've defined it.
     * Not necessarily a person's name.
     * Typically capped at 20 characters, but subject to change.
     *
     * @return string
     */
    public function getName()
    {
        return $this->data->name;
    }

    /**
     * The screen name, handle, or alias that this user identifies themselves with.
     * screen_names are unique but subject to change.
     * Use id_str as a user identifier whenever possible.
     * Typically a maximum of 15 characters long, but some historical accounts may exist with longer names.
     *
     * @return string
     */
    public function getScreenName()
    {
        return $this->data->screen_name;
    }

    /**
     * The number of users this account is following (AKA their "followings").
     * Under certain conditions of duress, this field will temporarily indicate "0."
     *
     * @return int
     */
    public function getFriendsCount()
    {
    	return $this->data->friends_count;
    }

    /**
     * Nullable.
     * The user-defined UTF-8 string describing their account.
     *
     * @return string
     */
    public function getDescription()
    {
    	return $this->data->description;
    }

    public function & rawData()
    {
        return $this->data ;
    }

    /**
     * Create an User object form a Json object.
     * 
     * @param mixed $data            
     * @return \Twitter\User
     */
    public static function createFrom($data)
    {
        $user = new User();
        $user->data = $data ;
        return $user;
    }
}