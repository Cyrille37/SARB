#!/usr/bin/env php
<?php
require_once ('Config.php');
require_once ('Twitter/TwitterBot.php');
require_once ('Lib/idiorm.php');

/**
 * Undocumented class
 */
class TwitterSlurp
{
    /**
     * @var string
     */
    protected $secretsFilename ;
    /**
     * @var \Twitter\TwitterBot
     */
    protected $tBot ;
    protected $userScreenName ;

    public function __construct()
    {
        $this->initFromCommandLine();

        $authData = Config::readSecretFile($this->secretsFilename);
        $this->tBot = new Twitter\TwitterBot( $authData['oauthConsumerKey'], $authData['oauthConsumerSecret'] );
        $this->tBot->setAccessToken( $authData['userId'], $authData['oauthToken'], $authData['oauthTokenSecret'] );

        $this->run();
    }

    protected function initFromCommandLine()
    {
        $opts = getopt('c:u:');

        if (! isset($opts['c']) || ! file_exists($opts['c'])) {
            die('Must have a configuration file (-c ConfigFilename)' . "\n");
        }
        $this->secretsFilename = $opts['c'] ;

        if( ! isset($opts['u']) || empty($opts['u'])) {
            die('Must have give an User Screename' . "\n");
        }
        $this->userScreenName = $opts['u'] ;
    }

    protected function run()
    {
        $user = $this->tBot->getUserByScreenName( $this->userScreenName );
        //echo json_encode( $user->rawData() );

        echo 'ScreenName: ', $user->getScreenName(),"\n";
        echo 'Name: ', $user->getName(),"\n";        
        echo 'Created At: ', $user->getCreatedAt()->format('c'),"\n";
        
    }
}

$ts = new TwitterSlurp();
